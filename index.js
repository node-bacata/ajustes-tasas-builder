const CsvProcessor = require('./src/Logic/CSVProcessor')
const ScriptsBuilder = require('./src/Logic/ScriptsBuilder')

 const main =  async () => {

   console.log('**** Iniciando ****')
    const csvProcessor = new CsvProcessor();
    const scriptsBuilder = new ScriptsBuilder();
    let dataCsv = await csvProcessor.readCsv('./tasas.csv');
    await scriptsBuilder.buildScripts(dataCsv);
   console.log('**** Terminado ****')
   


 }


 main();