const ScoreAcierta = {
    1: {min: '0',  max: '769', inExcel: '3 {< 770 y otros}'},
    2: {min: '770',  max: '1000', inExcel: '4 {>= 770}'}
}

module.exports = ScoreAcierta;