const Term = {
    13: {inExcel: '13 {Hasta 60}', max: 60},
    12: {inExcel: '12 {Entre 61 y 84}', min: 61, max: 84},
    11: {inExcel: '11 {Mas de 240}', min: 240, max: 360},
    10: {inExcel: '10 {Entre 181 y 240}', min: 181, max: 240},
    9:  {inExcel: '9 {Entre 121 y 180}', min:121, max: 180},
    8:  {inExcel: '8 {Entre 85 y 120}', min:85, max: 120},
}

module.exports = Term;