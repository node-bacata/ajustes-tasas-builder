const CreditLine = require('../Enums/CreditLine')
const FamilySegment = require('../Enums/FamilySegment')
const HousingType = require('../Enums/HousingType')
const ScoreAcierta = require('../Enums/ScoreAcierta')
const ClientType = require('../Enums/ClientType')
const Term = require('../Enums/Term')
const SQLBuilder = require('./SQLBuilder')





class ScriptsBuilder {


    async buildScripts(data) {
        let scriptsData = this.buildScriptsData(data);
        const sQLBuilder = new SQLBuilder();
        let date = new Date().getTime();
        await sQLBuilder.createInsert(`out/insert_housing_rates-${date}.sql`, scriptsData.insert);
        await sQLBuilder.createRollBack(`out/insert_housing_rates-${date}_RB.sql`, scriptsData.rollback);

    }

    buildScriptsData(data) {
        let insertValues = [];
        let rollBackKeys = []

        data.forEach(row => {
            let creditLine = this.buildCreditLine(row.LineasdeCredito);
            let familySegment = this.getFamilySegment(row.FamiliaSegmento);
            let housingType = this.getHousingType(row.TipoVivienda);
            let fundedProject = 2;
            let scoreAcierta = this.getScoreAcierta(row.Acierta);
            let clientType = this.getClientType(row.TipoCliente);
            let term = this.getTerm(row.PlazoMes);


            let key = `${creditLine.key}_${creditLine.creditLine}_${familySegment}_${housingType}_${fundedProject}_${scoreAcierta.max}_${clientType}_${term.max}`;
            let filters = this.buildFilters(creditLine,familySegment,housingType,fundedProject,scoreAcierta,clientType,term);
            let rates = this.buildRates(row.TasaMesInteligente, row.TasaMaxima);
            let effectiveDate = '2022-12-05 23:59:59'

            let insertData = `('${key}', '${filters}', '${rates}', '${effectiveDate}', CURRENT_TIMESTAMP)`;

            rollBackKeys.push(key);
            insertValues.push(insertData);

        })
        return {insert: insertValues, rollback: rollBackKeys}
    }

    buildCreditLine(creditLine) {
        let returnValue;
        Object.keys(CreditLine).forEach((value) => {
            let valueEnum = CreditLine[value];
            if(valueEnum.inExcel === creditLine) {
                returnValue = {key: valueEnum.inScript, creditLine: value}
            }
        })
        return returnValue;
    }

    getFamilySegment(familySegment) {
        let returnValue;
        Object.keys(FamilySegment).forEach((value) => {
            let valueEnum = FamilySegment[value];
            if(valueEnum === familySegment) {
                returnValue = value;
            }
        })
        return returnValue;
    }

    getHousingType(housingType) {
        let returnValue;
        Object.keys(HousingType).forEach((value) => {
            let valueEnum = HousingType[value];
            if(valueEnum === housingType) {
                returnValue = value;
            }
        })
        return returnValue;
    }

    getScoreAcierta(score) {
        let returnValue;
        Object.keys(ScoreAcierta).forEach((value) => {
            let valueEnum = ScoreAcierta[value];
            if(valueEnum.inExcel === score) {
                returnValue = {min: valueEnum.min, max: valueEnum.max}
            }
        })
        return returnValue;
    }

    getClientType(clientType) {
        let returnValue;
        Object.keys(ClientType).forEach((value) => {
            let valueEnum = ClientType[value];
            if(valueEnum === clientType) {
                returnValue = value;
            }
        })
        return returnValue;
    }

    getTerm(term) {
        let returnValue;
        Object.keys(Term).forEach((value) => {
            let valueEnum = Term[value];
            if(valueEnum.inExcel === term) {
                returnValue = { min: valueEnum.min, max: valueEnum.max }
            }
        })
        return returnValue;
    }

    buildFilters(creditLine, familySegment,housingType,fundedProject,scoreAcierta,clientType,term) {
        return JSON.stringify({creditLine:  creditLine.creditLine ,familySegment,housingType,fundedProject,scoreAcierta,clientType,term})
    }

    buildRates(smartMonthRate, maximumRate) {
        return JSON.stringify({smartMonthRate, maximumRate })
    }
}

module.exports = ScriptsBuilder;