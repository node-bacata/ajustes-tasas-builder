const fs = require("fs");
const csvParser = require("csv-parser");

class CsvProcessor {

    constructor() {}

    async readCsv(path) {
        const dataCsv = []
        return new Promise((resolve) => {
            fs.createReadStream(path)
            .pipe(csvParser())
            .on("data", (data) => {dataCsv.push(data);})
            .on("end", () => {
                 resolve(dataCsv);
                });
        });
    }
}

module.exports = CsvProcessor;