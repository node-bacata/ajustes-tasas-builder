const fs = require ( 'fs' );

const beginWorkSentence= 'BEGIN WORK; \n';
const deleteSentence = 'DELETE FROM bdb_rates.hc_rates WHERE key = '
const commitSentence = 'COMMIT; \n'
const insertSentence = 'INSERT INTO bdb_rates.hc_rates (key, filters, rates, effective_date, last_modified_date) VALUES \n'
class SQLBuilder {


    Constructor() {};

    createFile(filePath, content) {
        fs.writeFile(filePath, content, err => {});
    }

    async createInsert(filePath, data) {
        let cont = 0;
        this.createFile(filePath, insertSentence );
        await data.forEach(row => {
            if(cont === 90)
            {
                this.appendFile(filePath, insertSentence)
                cont = 0;
            }
            this.appendFile(filePath, `${row}, \n`)
            cont ++;
        })
    }

    appendFile(filePath, data) {
        fs.appendFile(filePath, data, err => {})
    }

    async createRollBack(filePath, data) {
        let cont = 0;
        this.createFile(filePath, beginWorkSentence);
        await data.forEach(row => {
            if(cont === 90) {
                this.appendFile(filePath, commitSentence)
                this.appendFile(filePath, beginWorkSentence);
                cont = 0;
            }
            this.appendFile(filePath, `${deleteSentence} '${row}'; \n`)
            cont ++;
        })
        this.appendFile(filePath, commitSentence,)
    }

}

module.exports = SQLBuilder;